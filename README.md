# Getting Started

### Testing the solution
* start two workers - one on port 8089, one on port 8090
* start application by using `./gradlew bootRun`
* to view the metric chart open metrics.html file in browser
    * data is refetch every 2 seconds an data is drawn
