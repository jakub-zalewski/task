package com.jakub.task.domain;

import com.jakub.task.adapters.repository.InMemoryMetricsRepository;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FetchMetricsTest {

    @Test
    public void shouldConsumeAndSaveMetrics() {
        // given
        Worker worker = new Worker(URI.create("http://localhost"));
        Metrics<MemoryUsage> stubbedMetrics = new Metrics<>(worker, List.of(new Metric<>("124", new MemoryUsage("124"))));

        MetricRepository metricRepository = setUpEnv(worker, stubbedMetrics);

        Awaitility.await().atMost(5, TimeUnit.SECONDS).until(() -> metricRepository.find().size() == 1);

        // when
        List<Metrics<MemoryUsage>> metrics = metricRepository.find();

        // then
        assertEquals(1, metrics.size());
        Metrics<MemoryUsage> fetchMetrics = metrics.get(0);
        // TODO this works because objects the same - write proper equals
        assertEquals(stubbedMetrics.getWorker(), fetchMetrics.getWorker());
        assertEquals(stubbedMetrics.getMetrics(), fetchMetrics.getMetrics());
    }

    private MetricRepository setUpEnv(Worker worker, Metrics<MemoryUsage> stubbedMetrics) {
        BlockingQueue<Metrics<MemoryUsage>> queue = new LinkedBlockingQueue<>();
        MetricRepository metricRepository = new InMemoryMetricsRepository();
        new MetricProducer(
            List.of(worker),
            queue,
            new StubMetricsFetcher(stubbedMetrics),
            new FetchInterval(TimeUnit.SECONDS, 1)
        );
        new MetricConsumer(queue, metricRepository, 1);
        return metricRepository;
    }
}
