package com.jakub.task.domain;

public class StubMetricsFetcher implements MetricsFetcher {

    private final Metrics<MemoryUsage> stubbedData;

    public StubMetricsFetcher(Metrics<MemoryUsage> stubbedData) {
        this.stubbedData = stubbedData;
    }

    @Override
    public Metrics<MemoryUsage> getMetrics(Worker worker) {
        return stubbedData;
    }
}
