package com.jakub.task.api;

import com.jakub.task.domain.MemoryUsage;
import com.jakub.task.domain.Metrics;

import java.util.List;

public class Response {
    public final List<Metrics<MemoryUsage>> data;

    public Response(List<Metrics<MemoryUsage>> data) {
        this.data = data;
    }
}
