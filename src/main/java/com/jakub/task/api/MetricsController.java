package com.jakub.task.api;

import com.jakub.task.domain.MetricRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MetricsController {

    private final MetricRepository metricRepository;

    public MetricsController(MetricRepository metricRepository) {
        this.metricRepository = metricRepository;
    }

    @GetMapping("/metrics")
    @CrossOrigin(origins = "*")
    public ResponseEntity<Response> get() {
        // TODO should be properly mapped
        return ResponseEntity.ok(new Response(metricRepository.find()));
    }
}

