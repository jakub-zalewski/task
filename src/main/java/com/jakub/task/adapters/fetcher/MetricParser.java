package com.jakub.task.adapters.fetcher;

import com.jakub.task.domain.MemoryUsage;
import com.jakub.task.domain.Metric;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class MetricParser {

    private static final String METRIC_LINE_SEPARATOR = " ";

    private MetricParser() {
        //utility class
    }

    public static List<Metric<MemoryUsage>> parseStringMetrics(String responseData) {
        List<Metric<MemoryUsage>> metrics = new ArrayList<>();

        //assert ordered
        Arrays.asList(responseData.split("\n")).forEach(metricLine -> {
            String[] timestampWithMetric;
            timestampWithMetric = metricLine.split(METRIC_LINE_SEPARATOR);
            metrics.add(new Metric<>(timestampWithMetric[0], new MemoryUsage(timestampWithMetric[1])));
        }
        );
        return metrics;
    }
}
