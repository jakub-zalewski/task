package com.jakub.task.adapters.fetcher;

import com.jakub.task.domain.MemoryUsage;
import com.jakub.task.domain.Metric;
import com.jakub.task.domain.Metrics;
import com.jakub.task.domain.MetricsFetcher;
import com.jakub.task.domain.Worker;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class RemoteMetricsFetcher implements MetricsFetcher {

    private final RestTemplate restTemplate;

    public RemoteMetricsFetcher(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public Metrics<MemoryUsage> getMetrics(Worker worker) {
        ResponseEntity<String> response = restTemplate.getForEntity(worker.getAddress(), String.class);
        String responseData = response.getBody();

        List<Metric<MemoryUsage>> metrics = MetricParser.parseStringMetrics(responseData);

        return new Metrics<>(worker, metrics);
    }


}
