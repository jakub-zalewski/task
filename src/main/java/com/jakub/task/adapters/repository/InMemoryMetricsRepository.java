package com.jakub.task.adapters.repository;

import com.jakub.task.domain.MemoryUsage;
import com.jakub.task.domain.Metric;
import com.jakub.task.domain.MetricRepository;
import com.jakub.task.domain.Metrics;
import com.jakub.task.domain.Worker;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class InMemoryMetricsRepository implements MetricRepository {

    private final Map<Worker, List<Metric<MemoryUsage>>> repository = new ConcurrentHashMap<>();

    //TODO - loose approach to collect last metrics fetched from remote - append the exising ones
    @Override
    public void save(Metrics<MemoryUsage> metrics) {
        repository.put(metrics.getWorker(), metrics.getMetrics());
    }

    @Override
    public List<Metrics<MemoryUsage>> find() {
        return repository.entrySet().stream()
                .map(entry -> new Metrics<>(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }
}
