package com.jakub.task.configuration;

import com.jakub.task.domain.FetchInterval;
import com.jakub.task.domain.MemoryUsage;
import com.jakub.task.domain.MetricConsumer;
import com.jakub.task.domain.MetricProducer;
import com.jakub.task.domain.MetricRepository;
import com.jakub.task.domain.Metrics;
import com.jakub.task.domain.MetricsFetcher;
import com.jakub.task.domain.Worker;
import com.jakub.task.adapters.fetcher.RemoteMetricsFetcher;
import com.jakub.task.adapters.repository.InMemoryMetricsRepository;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PreDestroy;
import java.net.URI;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

@Configuration
public class BeanConfiguration {

    public static final int NO_CONSUMERS = 2;

    @Bean
    MetricsFetcher metricsFetcher(RestTemplate restTemplate) {
        return new RemoteMetricsFetcher(restTemplate);
    }

    @Bean
    RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    BlockingQueue<Metrics<MemoryUsage>> queue() {
        return new LinkedBlockingDeque<>();
    }

    @Bean
    MetricProducer metricFetchingJob(MetricsFetcher metricsFetcher, BlockingQueue<Metrics<MemoryUsage>> queue) {
        return new MetricProducer(
            Arrays.asList(
                new Worker(URI.create("http://localhost:8089")),
                new Worker(URI.create("http://localhost:8090"))
            ),
            queue,
            metricsFetcher,
            new FetchInterval(TimeUnit.SECONDS, 2)
        );
    }

    @Bean
    MetricRepository metricRepository() {
        return new InMemoryMetricsRepository();
    }

    @Bean
    MetricConsumer metricConsumer(MetricRepository metricRepository, BlockingQueue<Metrics<MemoryUsage>> queue) {
        return new MetricConsumer(queue, metricRepository, NO_CONSUMERS);
    }

    @PreDestroy
    public void shutdownWorkers() {

    }
}
