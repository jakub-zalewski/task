package com.jakub.task.domain;

import java.util.List;

public class Metrics<T> {
    private final Worker worker;
    private final List<Metric<T>> metrics;

    public Metrics(Worker worker, List<Metric<T>> metrics) {
        this.worker = worker;
        this.metrics = metrics;
    }

    public Worker getWorker() {
        return worker;
    }

    public List<Metric<T>> getMetrics() {
        return metrics;
    }
}
