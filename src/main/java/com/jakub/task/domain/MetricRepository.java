package com.jakub.task.domain;

import java.util.List;

//TODO could be done generic
public interface MetricRepository {
    void save(Metrics<MemoryUsage> metrics);

    //TODO - allow searching - by worker, time
    List<Metrics<MemoryUsage>> find();
}
