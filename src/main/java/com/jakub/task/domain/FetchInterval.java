package com.jakub.task.domain;

import java.util.concurrent.TimeUnit;

public class FetchInterval {
    private final TimeUnit timeUnit;
    private final long value;

    public FetchInterval(TimeUnit timeUnit, long value) {
        this.timeUnit = timeUnit;
        this.value = value;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public long getValue() {
        return value;
    }
}
