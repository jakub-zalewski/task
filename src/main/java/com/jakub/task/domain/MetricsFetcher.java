package com.jakub.task.domain;

public interface MetricsFetcher {
    Metrics<MemoryUsage> getMetrics(Worker worker);
}
