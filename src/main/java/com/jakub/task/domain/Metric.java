package com.jakub.task.domain;

import java.util.Objects;

public class Metric<T> {
    private final String timestamp;
    private final T value;

    public Metric(String timestamp, T value) {
        this.timestamp = timestamp;
        this.value = value;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public T getValue() {
        return value;
    }
}
