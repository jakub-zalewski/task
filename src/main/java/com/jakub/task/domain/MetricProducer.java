package com.jakub.task.domain;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class MetricProducer {

    private final ScheduledExecutorService executorService;
    private final List<Worker> workers;
    private final MetricsFetcher metricsFetcher;
    private final BlockingQueue<Metrics<MemoryUsage>> queue;
    private final FetchInterval fetchInterval;

    public MetricProducer(
        List<Worker> workers,
        BlockingQueue<Metrics<MemoryUsage>> blockingQueue,
        MetricsFetcher metricsFetcher,
        FetchInterval fetchInterval
    ) {

        executorService = buildExecutorService(workers);

        this.workers = workers;
        this.metricsFetcher = metricsFetcher;
        this.queue = blockingQueue;
        this.fetchInterval = fetchInterval;

        // TODO think of this
        this.startFetching();
    }

    // TODO - should be moved to configuration
    private ScheduledExecutorService buildExecutorService(List<Worker> workers) {
        final ScheduledExecutorService executorService;
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        int corePoolSize = Math.min(workers.size(), availableProcessors);
        executorService = Executors.newScheduledThreadPool(corePoolSize);
        return executorService;
    }

    private void startFetching() {
        System.out.println("start fetching");
        workers.forEach(worker -> {
            executorService.scheduleWithFixedDelay(() -> {
                System.out.println("fetching metrics for worker: " + worker.getAddress());
                try {
                    queue.put(metricsFetcher.getMetrics(worker));
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            },
            fetchInterval.getValue(),
            fetchInterval.getValue(),
            fetchInterval.getTimeUnit());
        });
    }

}
