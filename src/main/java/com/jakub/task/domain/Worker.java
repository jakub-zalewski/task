package com.jakub.task.domain;

import java.net.URI;

public class Worker {
    private final URI address;

    public Worker(URI address) {
        this.address = address;
    }

    public URI getAddress() {
        return address;
    }
}
