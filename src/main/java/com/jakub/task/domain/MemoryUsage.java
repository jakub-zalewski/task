package com.jakub.task.domain;

public class MemoryUsage {
    private final String value;

    public MemoryUsage(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
