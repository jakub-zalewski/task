package com.jakub.task.domain;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MetricConsumer {

    private final BlockingQueue<Metrics<MemoryUsage>> queue;
    private final ExecutorService executorService;
    private final MetricRepository metricRepository;
    private final int noConsumers;

    public MetricConsumer(
        BlockingQueue<Metrics<MemoryUsage>> queue,
        MetricRepository metricRepository,
        int noConsumers
    ) {
        this.queue = queue;
        this.executorService = configureExecutorsService();
        this.metricRepository = metricRepository;
        this.noConsumers = noConsumers;
        startConsumption();
    }

    //TODO move to configuration
    private ExecutorService configureExecutorsService() {
        return Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    }

    private void startConsumption() {
        executorService.submit(() -> {
            try {
                while (true) {
                    if (Thread.currentThread().isInterrupted()) {
                        System.out.println("stopping consumption");
                        break;
                    }
                    Metrics<MemoryUsage> metrics = queue.take();
                    System.out.println("consuming metrics for worker: " + metrics.getWorker().getAddress());
                    metricRepository.save(metrics);
                    //todo graceful stop
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        });
    }
}
